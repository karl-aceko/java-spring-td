package com.example.leagueApplication.saison;

import com.example.leagueApplication.MySqlConnection;

public class CreerSaisonUseCase {


    private final Saison saison;
    private final SaisonDao saisonDao;

    public CreerSaisonUseCase(Saison saison) {
        this.saison = saison;
        this.saisonDao = new SaisonDao(MySqlConnection.getInstance());
    }

    public void ajoutSaison() {
        boolean saisonExisteDeja = this.saisonDao.verifierSiSaisonExiste(saison.getIdentifiant());
        if (saisonExisteDeja) {
            System.out.println("Impossible de créé la saison car elle existe déjà");
            return;
        }
        this.saisonDao.insererSaison(saison);
    }
}
