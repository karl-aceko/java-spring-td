package com.example.leagueApplication.saison;

import com.example.leagueApplication.MySqlConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SaisonDao {

    private final Connection connection;

    public SaisonDao(Connection connection) {
        this.connection = connection;
    }

    public void insererSaison(Saison saison) {
        String sql = "INSERT INTO saison (identifiant, libelle) VALUES (?,?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, saison.getIdentifiant());
            statement.setString(2, saison.getLibelle());
            statement.executeUpdate();
            System.out.println("La saison a été insérée avec succès.");
        } catch (SQLException e) {
            System.out.println("Erreur lors de l'insertion de la saison : " + e.getMessage());
        }
    }

    public void deleteSaison(String saisonId) {
        String sqlSuppressionSaison = "DELETE FROM saison WHERE id = ?";
        try (PreparedStatement statementSuppression = connection.prepareStatement(sqlSuppressionSaison)) {
            statementSuppression.setString(1, saisonId);
            int lignesAffectees = statementSuppression.executeUpdate();

            if (lignesAffectees > 0) {
                System.out.println("La saison a été supprimée avec succès.");
            } else {
                System.out.println("Aucune saison trouvée avec l'identifiant spécifié.");
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la suppression de la saison : " + e.getMessage());
        }
    }

    public boolean verifierSiSaisonAvecJourneesProgrammees(String saisonId) {
        String sqlCompterJournees = "SELECT COUNT(*) FROM journee WHERE identifiant_saison = ?";

        try (PreparedStatement statementComptage = connection.prepareStatement(sqlCompterJournees)) {
            statementComptage.setString(1, saisonId);
            ResultSet resultSet = statementComptage.executeQuery();

            if (resultSet.next()) {
                int nombreJournees = resultSet.getInt(1);
                return nombreJournees > 0;
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la vérification des journées programmées : " + e.getMessage());
        }

        return false;
    }
    public boolean verifierSiSaisonExiste(String saisonIdentifiant) {
        String sqlSaisonExiste = "SELECT COUNT(*) AS count FROM saison WHERE identifiant = ?";
        int count = 0;
        try (PreparedStatement statementComptage = connection.prepareStatement(sqlSaisonExiste)) {
            statementComptage.setString(1, saisonIdentifiant);
            ResultSet resultSet = statementComptage.executeQuery();

            if (resultSet.next()) {
                count = resultSet.getInt("count");
            }
        } catch (SQLException e) {
            System.out.println("Erreur la saison existe" + e.getMessage());
        }

        return count >0;
    }



}
