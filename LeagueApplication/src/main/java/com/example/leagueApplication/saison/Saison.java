package com.example.leagueApplication.saison;
import com.example.leagueApplication.jours.Journee;
import java.util.List;



public class Saison {
    private int saisaonId;;
    private String libelle;
    private String identifiant;
    private static List<Journee> journees;
    public Saison(String identifiant, String libelle) {
        this.identifiant = identifiant;
        this.libelle = libelle;
    }

    public int getSaisaonId() {
        return saisaonId;
    }

    public String getLibelle() {
        return libelle;
    }



    public String getIdentifiant() {
        return identifiant;
    }
}
