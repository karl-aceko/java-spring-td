package com.example.leagueApplication.saison;

import com.example.leagueApplication.MySqlConnection;

public class SupprimerSaisonUseCase {

    private Saison saison;
    private String identifiant;
    private final SaisonDao saisonDao;
    public SupprimerSaisonUseCase(String identifiant) {
        this.identifiant = identifiant;
        this.saisonDao = new SaisonDao(MySqlConnection.getInstance());
    }
    public void supprimerSaison() {
        boolean aJourneesProgrammees = this.saisonDao.verifierSiSaisonAvecJourneesProgrammees(saison.getIdentifiant());
        if (aJourneesProgrammees) {
            System.out.println("Impossible de supprimer la saison. Des journées sont programmées.");
            return;
        }
        this.saisonDao.deleteSaison(saison.getIdentifiant());
    }
}
