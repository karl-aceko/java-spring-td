package com.example.leagueApplication;

//@RestController
//@RequestMapping("/saisons")
public class Controller {
/*
    private List<Saison> saisonAncs = new ArrayList<>();

    @PostMapping("/")
    public ResponseEntity<String> createSaison(@RequestBody Saison saisonAnc) {
        if (isSaisonExists(saisonAnc)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("La saison existe déjà");
        }

        saisonAncs.add(saisonAnc);
        return ResponseEntity.status(HttpStatus.CREATED).body("La saison a été créée avec succès");
    }

    @DeleteMapping("/{identifiant}")
    public ResponseEntity<String> deleteSaison(@PathVariable("identifiant") String identifiant) {
        Saison saisonAnc = getSaisonByIdentifiant(identifiant);
        if (saisonAnc == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("La saison n'existe pas");
        }

        if (isJourneeProgrammee(saisonAnc)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Impossible de supprimer la saison car des journées sont programmées");
        }

        saisonAncs.remove(saisonAnc);
        return ResponseEntity.status(HttpStatus.OK).body("La saison a été supprimée avec succès");
    }

    @PostMapping("/{identifiant}/journees")
    public ResponseEntity<String> createJournee(@PathVariable("identifiant") String identifiant, @RequestBody Journee journee) {
        Saison saisonAnc = getSaisonByIdentifiant(identifiant);
        if (saisonAnc == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("La saison n'existe pas");
        }

        if (isJourneeExists(saisonAnc, journee.getNumero())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("La journée existe déjà dans cette saison");
        }

        saisonAnc.getJournees().add(journee);
        return ResponseEntity.status(HttpStatus.CREATED).body("La journée a été créée avec succès");
    }

    @PostMapping("/{identifiant}/journees/{numero}/matchs")
    public ResponseEntity<String> createMatch(@PathVariable("identifiant") String identifiant, @PathVariable("numero") int numero, @RequestBody Match match) {
        Saison saisonAnc = getSaisonByIdentifiant(identifiant);
        if (saisonAnc == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("La saison n'existe pas");
        }

        Journee journee = getJourneeByNumero(saisonAnc, numero);
        if (journee == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("La journée n'existe pas dans cette saison");
        }

        if (isMatchExists(journee, match)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Le match existe déjà dans cette journée");
        }

        journee.getMatchs().add(match);
        return ResponseEntity.status(HttpStatus.CREATED).body("Le match a été ajouté avec succès à la journée");
    }

    private boolean isSaisonExists(Saison saisonAnc) {
        return saisonAncs.stream().anyMatch(s -> s.getIdentifiant().equals(saisonAnc.getIdentifiant()));
    }

    private Saison getSaisonByIdentifiant(String identifiant) {
        return saisonAncs.stream().filter(s -> s.getIdentifiant().equals(identifiant)).findFirst().orElse(null);
    }

    private boolean isJourneeProgrammee(Saison saisonAnc) {
        // Logique pour vérifier si des journées sont programmées pour la saison donnée
        // Retourne true si des journées sont programmées, sinon false
        return false;
    }

    private boolean isJourneeExists(Saison saisonAnc, int numero) {
        return saisonAnc.getJournees().stream().anyMatch(j -> j.getNumero() == numero);
    }

    private Journee getJourneeByNumero(Saison saisonAnc, int numero) {
        return saisonAnc.getJournees().stream().filter(j -> j.getNumero() == numero).findFirst().orElse(null);
    }

    private boolean isMatchExists(Journee journee, Match match) {
        return journee.getMatchs().stream().anyMatch(m -> m.equals(match));
    }

    @GetMapping("/{identifiant}/journees/{numero}/matchs")
    public ResponseEntity<List<Match>> getMatchsJournee(@PathVariable("identifiant") String identifiant, @PathVariable("numero") int numero) {
        Saison saison = getSaisonByIdentifiant(identifiant);
        if (saison == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        Journee journee = getJourneeByNumero(saison, numero);
        if (journee == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        List<Match> matchs = journee.getMatchs();
        return ResponseEntity.status(HttpStatus.OK).body(matchs);
    }*/
}

