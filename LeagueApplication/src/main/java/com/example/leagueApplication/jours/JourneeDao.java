package com.example.leagueApplication.jours;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JourneeDao {
    private final Connection connection;
    public JourneeDao(Connection connection) {
        this.connection = connection;
    }

    public void insertJournee(Journee journee) {

            String sql = "INSERT INTO journee (numero,saison_id  ) VALUES (?,?)";

            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, journee.getNumero());
                statement.setString(2,journee.getSaisonIdentifiant());
                statement.executeUpdate();
                System.out.println("La saison a été insérée avec succès.");
            } catch (SQLException e) {
                System.out.println("Erreur lors de l'insertion de la saison : " + e.getMessage());
            }
        }

    }


