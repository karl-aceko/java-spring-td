package com.example.leagueApplication.jours;

import com.example.leagueApplication.match.Match;
import com.example.leagueApplication.saison.Saison;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Journee {
    private int id;
    private int numero;
    private String saisonIdentifiant;
    public String getSaisonIdentifiant() {
        return saisonIdentifiant;
    }
    public Journee( int numero, String saisonIdentifiant) {
        this.numero = numero;
        this.saisonIdentifiant = saisonIdentifiant;
    }
    public int getNumero() {
        return numero;
    }
}

