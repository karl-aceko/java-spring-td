package com.example.leagueApplication.jours;

import com.example.leagueApplication.MySqlConnection;
import com.example.leagueApplication.saison.SaisonDao;

import java.sql.Connection;

public class CreerJourneeUseCase {
    private final Journee journee;
    private final JourneeDao journeeDao;
    private final SaisonDao saisonDao;

    public CreerJourneeUseCase(Journee journee) {
        this.journee = journee;
        this.journeeDao = new JourneeDao(MySqlConnection.getInstance());
        this.saisonDao = new SaisonDao(MySqlConnection.getInstance());
    }


    public void ajoutJournee() {
        boolean saisonEntreExiste = this.saisonDao.verifierSiSaisonExiste(journee.getSaisonIdentifiant());
        if (saisonEntreExiste) {
            this.journeeDao.insertJournee(journee);
        return;
        }
            System.out.println("Les données entré pour la création de la journnée sont a revoir.");
    }
}
