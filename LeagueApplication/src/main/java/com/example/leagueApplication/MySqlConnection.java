package com.example.leagueApplication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnection {


    static String url = "jdbc:mysql://localhost:3306/league";
    static String username = "root";
    static String password = "root1234";

    public static Connection getInstance() {
        try {
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
