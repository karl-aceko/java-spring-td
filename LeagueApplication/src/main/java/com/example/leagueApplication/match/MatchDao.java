package com.example.leagueApplication.match;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MatchDao {
    private final Connection connection;

    public MatchDao(Connection connection) {
        this.connection = connection;
    }

    public void insertMatch(Match match) {
            String sqlCreeMatch = "INSERT INTO matchs (equipe_domicile, equipe_exterieur  ) VALUES (?,?)";
            try (PreparedStatement statement = connection.prepareStatement(sqlCreeMatch)) {
                statement.setString(1, match.getEquipeDomicile());
                statement.setString(2, match.getEquipeExterieur());
                statement.executeUpdate();
                System.out.println("Le match a été insérée avec succès.");
            } catch (SQLException e) {
                System.out.println("Erreur lors de l'insertion du match : " + e.getMessage());
            }
        }
    public boolean VerifieJourneeEtSaisonExistes(int numeroJour, String identifiantSaison) {
        String sqlSaisonExiste = "SELECT COUNT(*) AS count FROM journee WHERE numero = ? AND id_saison = ?";;
        int count = 0;
        try (PreparedStatement statement = connection.prepareStatement(sqlSaisonExiste)) {
            statement.setString(1, identifiantSaison);
            statement.setInt(2, numeroJour);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt("count");
                return count >0;
            }
        } catch (SQLException e) {
            System.out.println("Erreur , La saison ou la journéé dans laquelle vous voullez créé le match n'existe pas." + e.getMessage());
        }
        return true;
    }

    public boolean VerifieEquipeExistes(String equipeDomicile, String equipeExterieur) {
        String sqlEquipeExiste = "SELECT COUNT(*) AS count FROM equipe WHERE nom = ? AND (SELECT COUNT(*) FROM equipe WHERE nom = ?) > 0";
        int count = 0;
        try (PreparedStatement statement = connection.prepareStatement(sqlEquipeExiste)) {
            statement.setString(1, equipeDomicile);
            statement.setString(2, equipeExterieur);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt("count");
                return count >0;
            }
        } catch (SQLException e) {
            System.out.println("Erreur " + e.getMessage());
        }
        return true;
    }
}
