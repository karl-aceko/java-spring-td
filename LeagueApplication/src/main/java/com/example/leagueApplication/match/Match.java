package com.example.leagueApplication.match;
import java.time.LocalDateTime;
public class Match {

    private String identifiantSaison;
    private int numeroJour;
    private String equipeDomicile;
    private String equipeExterieur;
    private LocalDateTime horaire;
    private boolean termine;

    public String getIdentifiantSaison() {
        return identifiantSaison;
    }
    public int getNumeroJour() {
        return numeroJour;
    }
    public Match(String identifiantSaison, int numeroJour, String equipeDomicile, String equipeExterieur) {
        this.identifiantSaison = identifiantSaison;
        this.numeroJour = numeroJour;
        this.equipeDomicile = equipeDomicile;
        this.equipeExterieur = equipeExterieur;
        this.termine = false;
    }
    public String getEquipeDomicile() {
        return equipeDomicile;
    }
    public String getEquipeExterieur() {
        return equipeExterieur;
    }
}
