package com.example.leagueApplication.match;
import com.example.leagueApplication.MySqlConnection;
import com.example.leagueApplication.match.MatchDao;

public class CreerMatchUseCase {
    private final Match match;
    private final MatchDao MatchDao;
    public CreerMatchUseCase(Match match) {
        this.match = match;
        this.MatchDao = new MatchDao(MySqlConnection.getInstance());
    }
    public void ajoutMatch() {
        boolean journeeEtSaisonExistes =  this.MatchDao.VerifieJourneeEtSaisonExistes(match.getNumeroJour(), match.getIdentifiantSaison());
        boolean equipeExiste = this.MatchDao.VerifieEquipeExistes(match.getEquipeDomicile(), match.getEquipeExterieur());
        if (journeeEtSaisonExistes && equipeExiste ) {
            this.MatchDao.insertMatch(match);
            return;
        }
        System.out.println("Les données inseré pour la création du match ne sont pas correct");
    }
}