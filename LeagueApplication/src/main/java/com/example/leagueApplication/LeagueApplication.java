package com.example.leagueApplication;

import com.example.leagueApplication.jours.Journee;
import com.example.leagueApplication.match.Match;
import com.example.leagueApplication.saison.Saison;
//import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication
public class LeagueApplication {

	public static void main(String[] args) {

		ConsoleInterface consoleInterface = new ConsoleInterface();
		consoleInterface.start();
	}

}
