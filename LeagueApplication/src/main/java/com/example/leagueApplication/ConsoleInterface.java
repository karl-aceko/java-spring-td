package com.example.leagueApplication;

import com.example.leagueApplication.equipe.CreerEquipeUseCase;
import com.example.leagueApplication.equipe.Equipe;
import com.example.leagueApplication.jours.CreerJourneeUseCase;
import com.example.leagueApplication.jours.Journee;
import com.example.leagueApplication.match.CreerMatchUseCase;
import com.example.leagueApplication.match.Match;
import com.example.leagueApplication.saison.CreerSaisonUseCase;
import com.example.leagueApplication.saison.Saison;
import com.example.leagueApplication.saison.SupprimerSaisonUseCase;


import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class ConsoleInterface {
    private final Scanner scanner;

    public ConsoleInterface() {
        this.scanner = new Scanner(System.in);
    }

    public void start() {
        System.out.println("Veuillez sélectionner votre action :");
        System.out.println("1 - Créer une saison");
        System.out.println("2 - Créer une équipe");
        System.out.println("3 - Créer une journée");
        System.out.println("4 - Créer un match");

        int choix = scanner.nextInt();
        scanner.nextLine();

        switch (choix) {
            case 1 -> creerSaison();
            case 2 -> creerEquipe();
            case 3 -> creerJournee();
            case 4 -> creerMatch();
            case 5 -> supprimerSaison();
            default -> System.out.println("Choix invalide");
        }
    }

    private void supprimerSaison() {
        System.out.println("Veuillez entrer l'année de la saison que vous voulez supprimer:");
        String identifiant = scanner.nextLine();;
        SupprimerSaisonUseCase useCase = new SupprimerSaisonUseCase(new SupprimerSaisonUseCase(identifiant).toString());
        useCase.supprimerSaison();
    }

    private void creerEquipe() {
        System.out.println("Veuillez entrer le nom de l'equipe que vous voulez créé :");
        String nomEquipe = scanner.nextLine();
        System.out.println("A quelle saison appartient il? le format doit être AAAA-AAAA");
        String identifianSaison = scanner.nextLine();
        CreerEquipeUseCase useCase = new CreerEquipeUseCase(new Equipe(nomEquipe, identifianSaison));
        useCase.ajoutEquipe();
        start();
    }

    private void creerSaison() {
        System.out.println("Veuillez entrer l'année de la saison :");
        String identifiant = scanner.nextLine();
        System.out.println("Veuillez entrer le libelle :");
        String libelle = scanner.nextLine();
        CreerSaisonUseCase useCase = new CreerSaisonUseCase(new Saison(identifiant,libelle));
        useCase.ajoutSaison();
        start();
    }

    private void creerJournee() {
        System.out.println("Veuillez saisir le numéro de la journée :");
        int numero = parseInt(scanner.nextLine());
        System.out.println("Dans quelle saison sera cette journée? ");
        String saisonIdentifiant = scanner.nextLine();
        CreerJourneeUseCase useCase = new CreerJourneeUseCase(new Journee(numero,saisonIdentifiant));
        useCase.ajoutJournee();
        start();
    }

    private void creerMatch() {
        System.out.println("Le match va se deroulé dans quelle saison?");
        String identifiantSaison = scanner.nextLine();
        System.out.println("Le match va se deroulé dans quelle journée?");
        int numeroJour = parseInt(scanner.nextLine());
        System.out.println("Veuillez entrer les id de l'équipe a domicile");
        String equipeDomicile = scanner.nextLine();
        System.out.println("Veuillez entrer les id de l'équipe a exterieur");
        String equipeExterieur = scanner.nextLine();
        CreerMatchUseCase useCase = new CreerMatchUseCase(new Match( identifiantSaison, numeroJour, equipeDomicile,equipeExterieur));
        useCase.ajoutMatch();
        start();
    }
}

