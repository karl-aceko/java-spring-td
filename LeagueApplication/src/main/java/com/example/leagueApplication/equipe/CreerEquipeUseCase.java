package com.example.leagueApplication.equipe;

import com.example.leagueApplication.MySqlConnection;
import com.example.leagueApplication.saison.SaisonDao;

public class CreerEquipeUseCase {

    private final Equipe equipe;
    private final EquipeDao equipeDao;
    private final SaisonDao saisonDao;

    public CreerEquipeUseCase(Equipe equipe) {
        this.equipe = equipe;
        this.equipeDao = new EquipeDao(MySqlConnection.getInstance());
        this.saisonDao = new SaisonDao(MySqlConnection.getInstance());
    }

    public void ajoutEquipe() {

        boolean saisonEntreExiste = this.saisonDao.verifierSiSaisonExiste(equipe.getIdentifiantSaison());
        if (saisonEntreExiste) {
            this.equipeDao.insertEquipe(equipe);
        }
        System.out.println("La n'existe pas veillé la créé");

    }
}
