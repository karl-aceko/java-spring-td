package com.example.leagueApplication.equipe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EquipeDao {

    private final Connection connection;
    public EquipeDao(Connection connection){this.connection = connection;}
    public void insertEquipe(Equipe equipe){

            String sql = "INSERT INTO equipe (nom,saison_id  ) VALUES (?,?)";

            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, equipe.getNomEquipe());
                statement.setString(2,equipe.getIdentifiantSaison());
                statement.executeUpdate();
                System.out.println("L'équipe a été créé avec succès.");
            } catch (SQLException e) {
                System.out.println("Erreur lors de l'insertion de la saison : " + e.getMessage());
            }
        }

}
