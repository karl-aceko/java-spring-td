package com.example.leagueApplication.equipe;

public class Equipe {
    int idEquipe;
    String nomEquipe;
    String identifiantSaison;

    public Equipe(String nomEquipe , String identifiantSaison) {
        this.nomEquipe = nomEquipe;
        this.identifiantSaison = identifiantSaison;

    }
    public String getNomEquipe() {
        return nomEquipe;
    }

    public String getIdentifiantSaison() {
        return identifiantSaison;
    }
}
